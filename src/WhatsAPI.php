<?php

namespace Hpsweb\WhatsAPI;

use Illuminate\Support\Facades\Log;

/**
 * touchSMS API class
 *
 * API Documentation: http://support.touchsms.com.au/hc/en-us/categories/200093809-APIs
 * TouchSMS Information: http://touchsms.com.au
 *
 * @author touchSMS <https://github.com/touchsms>
 * @version 1.3
 */
class WhatsAPI
{

    /**
     * The API base URL
     */
    const API_URL = 'http://localhost';
    const SANDBOXAPI_URL = 'http://eo1mt4knrkxko10.m.pipedream.net';

    /**d
     * The API Unique Id
     *
     * @var string
     */
    private $_unqiueId;

    private $api_url;

    private $_sandbox;
    private $_obj;
    private $_response;
    private $_message;
    private $_notice;

    /**
     * API Keys can be generated at https://platform.touchsms.com.au/apis/
     *
     * @param string $uniqueId          touchSMS unique id API key
     * @param string $uniquePassword    touchSMS unique password API key
     *
     * @return void
     */
    public function __construct($uniqueId, $sandbox = false)
    {
        $this->_unqiueId = $uniqueId;
        $this->_sandbox = $sandbox;
        $this->api_url = env('WHATSAPI_SERVER') ?? API_URL;
    }

    /**
     * Check the balance and get other information about the authenticated user.
     *
     * @return mixed
     */

    public function restartSession($sessionId)
    {
        $this->closeSession($sessionId);
        return $this->startSession($sessionId);
    }

    /**
     * Start session of authenticated user.
     *
     * @return mixed
     */

    public function startSession($sessionId, $options = [])
    {
        $arrayOpts = array_merge(['key' => "{$sessionId}"], $options);
        return $this->_makeRequest("/instance/init", 'POST', $arrayOpts);
    }

    /**
     * Close session of authenticated user.
     *
     * @return mixed
     */

    public function closeSession($sessionId)
    {
        return $this->_makeRequest("/instance/delete?key={$sessionId}", 'DELETE');
    }

    /**
     * Logout session of authenticated user.
     *
     * @return mixed
     */

    public function logoutSession($sessionId)
    {
        return $this->_makeRequest("/instance/logout?key={$sessionId}", "DELETE");
    }

    /**
     * QR Code session.
     *
     * @return mixed
     */

    public function qrSession($sessionId)
    {
        return $this->_makeRequest("/instance/qrbase64?key={$sessionId}", 'GET');
    }

    /**
     * Get session device data of authenticated user.
     *
     * @return mixed
     */

    public function deviceSession($sessionId)
    {
        return $this->_makeRequest("/{$sessionId}/host-device", 'GET');
    }

    /**
     * Get session device data of authenticated user.
     *
     * @return mixed
     */

    public function sessionInfo($sessionId)
    {
        return $this->_makeRequest("/instance/info?key={$sessionId}", 'GET');
    }

    /**
     * Check if number exists on WhatsApp.
     *
     * @return mixed
     */

    public function checkNumber($sessionId, $number)
    {
        $check = $this->_makeRequest("/misc/onwhatsapp?key={$sessionId}&id={$number}", 'GET');
        
        return !$check->error && $check->data ? true : false;
    }

    /**
     * Send an SMS to a mobile handset.
     *
     * @param string  $message              Non-Unicode message to be sent to the handset
     * @param integer $mobileNumber         Mobile number in international format. To send
     *                                      to the mobile 0491 570 110, submit 61491570110
     * @param string [optional]  $sessionId  When null the system will send a two
     *                                      way message from our shared-pool of mobile
     *                                      numbers. Optional to send from a valid Alpha/Numeric
     *                                      source.
     * @return mixed
     */

    public function sendMessage($message, $mobileNumber, $sessionId = null)
    {
        if (is_null($sessionId))
            return "Error";

        return $this->_makeRequest("/message/text?key={$sessionId}", 'POST', array('id' => "{$mobileNumber}", 'message' => $message));
    }

    /**
     * Get human readable responses from the last API call
     *
     * @return string
     */

    public function getCleanStatus()
    {
        return $this->_notice;
    }

    /**
     * Get human readable responses from the last API call
     *
     * @return string
     */

    public function getResponse()
    {
        return $this->_message;
    }

    /**
     * Make API curl request.
     *
     * @param string  $function             API resouce path
     * @param string  $method               Request type GET|POST|DELETE
     * @param array [optional] $params      Additional request parameters
     *
     * @return mixed
     */

    private function _makeRequest($function, $method, $params = array())
    {
        $client = new \GuzzleHttp\Client(['headers' => ["Accept" => "application/json", "Content-Type" => "application/json", "authorization" => "Bearer {$this->_unqiueId}"]]);

        if (is_array($params)) {
            $paramString = http_build_query($params);
            $payload = json_encode($params);
        }

        if ($this->_sandbox) {
            $apiUrl = self::SANDBOXAPI_URL;
        } else {
            $apiUrl = $this->api_url;
        }

        $apiCall = $apiUrl . $function . (($method == 'GET') ? $paramString : null);

        $options = [];

        if ($method == 'POST' || $method == 'PUT' || $method == 'DELETE')
            $options = array_merge(['body' => $payload], $options);

        try {
            $response = $client->request($method, $apiCall, $options);

            $httpcode = $response->getStatusCode();
            $jsonData = $response->getBody();

            $jsonDecode = json_decode($jsonData);
            // $this->_decodeResponse($jsonDecode, $httpcode);

            return $jsonDecode;
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $response = $e->getResponse();

            $httpcode = $response->getStatusCode();
            $jsonData = $response->getBody();

            $jsonDecode = json_decode($jsonData);
            // $this->_decodeResponse($jsonDecode, $httpcode);

            return $jsonDecode;
        }
    }

    /**
     * Decode JSON response
     *
     * @param json  $jsonData   JSON encoded string returned from the API
     *
     * @return null
     */

    private function _decodeResponse($jsonData, $httpcode)
    {
        $this->_obj = $jsonData;
        $this->_obj->statusCode = $httpcode;
        if ($this->_obj->statusCode === 201)
            $this->_message = $this->_obj->message;
        else
            $this->_message = "";

        $response = '';
        switch ($this->_obj->statusCode) {
            case 201 || 200:
                $this->_response = 'Success';
                $this->_notice = $this->_response;
                break;
            case 400:
                $this->_response = 'Bad Request';
                $this->_notice = $this->_response;
                // foreach ($this->_obj->errors->children as $errors) {
                //     foreach ($errors as $error) {
                //         $response .= join($error, ', ') . '. ';
                //     }
                // }

                // $this->_notice = rtrim(trim($response), '.') . '.';
                break;
            case 401:
                $this->_response = 'Login Required';
                $this->_notice = $this->_response;
                break;
            case 402:
                $this->_response = 'Payment Required';
                $this->_notice = $this->_response;
                break;
            case 403:
                $this->_response = 'Forbidden';

                foreach ($this->_obj->errors as $error) {
                    $response .= $error . '. ';
                }

                $this->_notice = rtrim(trim($response), '.') . '.';
                break;
            case 405:
                $this->_response = 'Method Not Allowed';
                $this->_notice = $this->_response;
                break;
            case 406:
                $this->_response = 'Not Acceptable';
                $this->_notice = $this->_response;
                break;
            case 409:
                $this->_response = 'Conflict';
                $this->_notice = $this->_response;
                break;
            default:
                $this->_response = 'Unhandled Response';
                $this->_notice = $this->_response;
                break;
        }
    }
}
