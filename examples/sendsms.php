<?php
require '../src/WhatsAPI.php';

use Hpsweb\WhatsAPI\WhatsAPI;

$touchSMS = new WhatsAPI('YOUR_API_ID', 'YOUR_API_PASSWORD');

/*
        Mobile number provided by ACMA
        http://www.acma.gov.au/Citizen/Consumer-info/All-about-numbers/Special-numbers/fictitious-numbers-for-radio-film-and-television-i-acma
    */


$obj = $touchSMS->sendMessage('Testando API do WhatsApp :)', '557191837508', 'session1');

if ($obj->statusCode == "001") {
    echo 'Message Sent';
} else {
    echo '[' . $obj->statusCode . '] ';
    echo $touchSMS->getCleanResponse();
}
